FROM adoptopenjdk/openjdk11:jdk11u-alpine-nightly-slim
VOLUME /tmp
ADD target/demo-web*.jar /app.jar
CMD ["java", "-jar","/app.jar","--spring.profiles.active=prod"]
EXPOSE 8080
#COPY . /usr/src/myapp
#WORKDIR /usr/src/myapp
#RUN javac Main.java
